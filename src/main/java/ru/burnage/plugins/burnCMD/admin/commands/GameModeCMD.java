package ru.burnage.plugins.burnCMD.admin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;

public class GameModeCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (args.length == 1) {
			if (sender instanceof Player) {
				Player player = (Player)sender;

				if (!player.hasPermission("burncmd.gamemode")) {
					player.sendMessage(ChatColor.RED + "У вас нет прав!");
					return true;
				}

				GameMode gameMode = gamemodeById(args[0]);
				player.setGameMode(gameMode);
				player.sendMessage(ChatColor.GREEN + "Ваш игровой режим изменен на" + ChatColor.YELLOW + gameMode.toString());
				return true;
			} else {
				sender.sendMessage(ChatColor.RED + "Укажите игрока");
			}
		} else if (args.length == 2) {
			if (!sender.hasPermission("burncmd.gamemode.other")) {
				sender.sendMessage(ChatColor.RED + "У вас нет прав!");
				return true;
			}

			Player targetPlayer = Bukkit.getPlayer(args[0]);
			if (targetPlayer == null) {
				sender.sendMessage(ChatColor.RED + "Игрок " + ChatColor.YELLOW + args[0] + ChatColor.RED + " не найден");
				return true;
			}

			GameMode gameMode = gamemodeById(args[1]);
			targetPlayer.setGameMode(gameMode);
			targetPlayer.sendMessage(ChatColor.GREEN + "Ваш игровой режим изменен на " + ChatColor.YELLOW + gameMode.toString());
			sender.sendMessage(ChatColor.GREEN + "Игровой режим игрока " + ChatColor.YELLOW+targetPlayer.getName() + ChatColor.GREEN + " изменен на " + ChatColor.YELLOW + gameMode.toString());
			return true;
		}

		return false;
	}

	private GameMode gamemodeById(String strId) {
		switch (strId) {
			case "1":
			case "creative":
				return GameMode.CREATIVE;
			case "2":
			case "adventure":
				return GameMode.ADVENTURE;
			case "3":
			case "spectator":
				return GameMode.SPECTATOR;
			default:
				return GameMode.SURVIVAL;
		}
	}
}
