package ru.burnage.plugins.burnCMD.admin;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class VanishManager {
	
	private static Set<String> playersInVanish = new HashSet<String>();
	
	public static void reset (){
		playersInVanish.clear();
	}
	
	public static void vanish (String name){
		if(isVanished(name)){
			found (name);
		} else {
			hide (name);
		}
	}
	
	private static void found(String name){
		Collection<? extends Player> players = Bukkit.getOnlinePlayers();
		Player inVanish = Bukkit.getPlayer(name.toLowerCase());
        playersInVanish.remove(name.toLowerCase());
		for (Player player : players){				
			player.showPlayer(inVanish);
            if (player.hasPermission("burncmd.vanish.see")){
                if(!player.getName().equals(name))
                    player.sendMessage(ChatColor.YELLOW + name + ChatColor.DARK_AQUA + " вышел из режима невидимости.");
            }
		}
		inVanish.sendMessage(ChatColor.DARK_AQUA + "Теперь вы видимы для других!");
		Bukkit.getLogger().info("Игрок "+ name + " вышел из режима невидимости!");

	}
	
	private static void hide (String name){
		Collection<? extends Player> players = Bukkit.getOnlinePlayers();
		Player inVanish = Bukkit.getPlayer(name.toLowerCase());
		for (Player player : players){
			if (!player.hasPermission("burncmd.vanish.see")){
				player.hidePlayer(inVanish);
			} else {
                if(!player.getName().equals(name))
				    player.sendMessage(ChatColor.YELLOW + name + ChatColor.DARK_AQUA + " вошел в режим невидимости .");
			}
		}
		inVanish.sendMessage(ChatColor.DARK_AQUA + "Вы стали невидимы для других!");
		Bukkit.getLogger().info("Игрок "+ name + " вошел в режим невидимости!");
		playersInVanish.add(name.toLowerCase());
	}
	
	public static boolean isVanished (String name){
        return playersInVanish.contains(name.toLowerCase());
	}
	
	public static void playerJoinManager(Player player){
        if(player.hasPermission("burncmd.vanish.vanish")){
            hide(player.getName());
        }
        if(player.hasPermission("burncmd.vanish.see")) {
            return;
        }
        if (playersInVanish.isEmpty())
                return;
        for (String name : playersInVanish) {
            if (Bukkit.getPlayer(name.toLowerCase()) != null)
                player.hidePlayer(Bukkit.getPlayer(name.toLowerCase()));
        }


	}
}
