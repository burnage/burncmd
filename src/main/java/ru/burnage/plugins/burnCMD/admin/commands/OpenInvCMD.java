package ru.burnage.plugins.burnCMD.admin.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OpenInvCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage(ChatColor.RED + "Недоступно из консоли");
            return true;
        }
        if (args.length != 1){
            return false;
        }
        if (!sender.hasPermission("burncmd.openinv")){
            sender.sendMessage(ChatColor.RED + "Нет прав на просмотр инвентаря.");
            return true;
        }

        Player player = (Player) sender;
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null){
            sender.sendMessage(ChatColor.RED + "Игрока нет онлайн");
            return true;
        }
        openInventory(player, target);
        return true;
    }

    private void openInventory (Player player, Player target){
        player.openInventory(target.getInventory());
    }
}
