package ru.burnage.plugins.burnCMD.admin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.admin.VanishManager;

public class VanishCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
		if (sender.hasPermission("burncmd.vanish.vanish")){
            if(arg3.length==1){
                if (arg3[0].toLowerCase().equals("status")){
                    if (VanishManager.isVanished(sender.getName())){
                        sender.sendMessage(ChatColor.AQUA + "Вы скрыты от всех.");
                    } else {
                        sender.sendMessage(ChatColor.AQUA + "Вы всем видны.");
                    }
                }
            } else {
                VanishManager.vanish(sender.getName());
            }


		} else {
			sender.sendMessage(ChatColor.RED + "У вас нет прав!");
		}
		return true;
	}
}
