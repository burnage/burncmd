package ru.burnage.plugins.burnCMD.other.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.other.GodManager;

public class GodCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(args.length == 0){
            if(sender.hasPermission("burncmd.god")) {
                if (!GodManager.isGod(sender.getName())) {
                    GodManager.setGod(sender.getName());
                    sender.sendMessage(ChatColor.GREEN + "Установлен режим бессмертия.");
                } else {
                    GodManager.removeGod(sender.getName());
                    sender.sendMessage(ChatColor.YELLOW + "Убран режим бессмертия.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Нет прав.");
            }
        } else if (args.length == 1){
            if(sender.hasPermission("burncmd.god.other")){
                Player p = Bukkit.getPlayer(args[0]);
                if (p != null){
                    String name = p.getName();
                    if (GodManager.isGod(name)) {
                        GodManager.removeGod(name);
                        p.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.RED + " снял с Вас режим бессмертия.");
                        sender.sendMessage(ChatColor.RED + "Режим бессмертия отключен у игрока " + ChatColor.YELLOW + p.getName());
                    } else {
                        GodManager.setGod(name);
                        p.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " установил Вам режим бессмертия.");
                        sender.sendMessage(ChatColor.GREEN + "Режим бессмертия установлен у игрока " + ChatColor.YELLOW + p.getName());
                    }
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Нет прав!");
            }
        } else {
            return false;
        }
        return true;
    }
}
