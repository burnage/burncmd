package ru.burnage.plugins.burnCMD.other;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class FlyManager {

    static Set<String> flyModePeople = new HashSet<String>();

    public static void fly (Player player){
        flyModePeople.add(player.getName());
        player.setAllowFlight(true);
        player.setFlying(true);
        player.sendMessage(ChatColor.GREEN + "Установлен режим полета!");
    }

    public static void stopFly (Player player){
        flyModePeople.remove(player.getName());
        player.setAllowFlight(false);
        player.setFlying(false);
        player.sendMessage(ChatColor.GREEN + "Убран режим полета!");
    }

    public static boolean isFly (Player player){
        return flyModePeople.contains(player.getName());
    }

    public static void changeFly(Player player){
        if (isFly(player)){
            stopFly(player);
        } else{
            fly(player);
        }
    }

    public static void clearSet (){
        flyModePeople.clear();
    }
}
