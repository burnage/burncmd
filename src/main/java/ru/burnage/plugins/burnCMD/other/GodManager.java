package ru.burnage.plugins.burnCMD.other;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class GodManager {
    static Set<String> godPlayers = new HashSet<String>();

    public static void setGod (String player){
        godPlayers.add(player.toLowerCase());
    }

    public static boolean isGod (String player){
        return godPlayers.contains(player.toLowerCase());
    }

    public static void removeGod (String player){
        godPlayers.remove(player.toLowerCase());
    }
}
