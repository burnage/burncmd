package ru.burnage.plugins.burnCMD.other.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.other.FlyManager;


public class FlyCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }

        if(sender.hasPermission("burncmd.fly")){
            FlyManager.changeFly(Bukkit.getPlayer(sender.getName()));
        } else{
            sender.sendMessage(ChatColor.RED + "У вас нет прав!");
        }

        return true;
    }
}
