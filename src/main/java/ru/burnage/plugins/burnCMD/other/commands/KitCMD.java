package ru.burnage.plugins.burnCMD.other.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.other.KitManager;

import java.util.HashSet;
import java.util.Set;

public class KitCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage(ChatColor.RED + "Команда недоступна из консоли.");
            return true;
        }

        if (args.length == 0){
            Set<String> kits = KitManager.getKits();
            Set<String> able = new HashSet<>();
            for (String kitname : kits){
                if (sender.hasPermission("burncmd.kit." + kitname.toLowerCase())){
                    able.add(kitname);
                }
            }
            if (able.size() == 0)
                sender.sendMessage(ChatColor.YELLOW + "Нет доступных наборов для Вас.");
            else {
                sender.sendMessage(ChatColor.YELLOW + "Вам доступны следующие наборы:");
                for (String kitname : able)
                    sender.sendMessage(ChatColor.YELLOW + "-" + kitname);
            }
            return true;
        }

        if (args.length != 1){
            return false;
        }
        KitManager.getKit(((Player) sender), args[0].toLowerCase());
        return true;
    }
}
