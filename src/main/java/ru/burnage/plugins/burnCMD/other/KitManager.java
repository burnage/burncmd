package ru.burnage.plugins.burnCMD.other;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class KitManager {
    private static Map<String, String> lastTaken = new HashMap<>(); // name#kitname :: stamp
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static Map <String, List <ItemStack>> kits = new HashMap<>();

    public static Date getLastTaken (String name, String kit){
        try {
            String last = lastTaken.get(String.format("%s#%s", name, kit));
            return SDF.parse(last);
        } catch (ParseException e) {
            return null;
        } catch (NullPointerException e){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.YEAR, -1);
            return calendar.getTime();
        }
    }

    public static Set<String> getKits (){
        return kits.keySet();
    }

    private static boolean canTake (String name, String kit){
        name = name.toLowerCase();
        kit = kit.toLowerCase();
        Date date = getLastTaken(name, kit);
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, -24);
        Date ago = now.getTime();
        return ago.after(date);
    }

    private static int timePassedHours (String name, String kit){
        name = name.toLowerCase();
        kit = kit.toLowerCase();
        Date date = getLastTaken(name, kit);
        Date now = Calendar.getInstance().getTime();
        Bukkit.getLogger().info(String.valueOf(now.getTime() - date.getTime()));
        return (int) (24-(now.getTime() - date.getTime())/1000/60/60);
    }

    public static boolean getKit (Player player, String kitname){

        try {

            if (canTake(player.getName(), kitname)) {
                if (!player.hasPermission("burncmd.kit." + kitname.toLowerCase())) {
                    player.sendMessage(ChatColor.RED + "Нет прав на набор " + kitname + ".");
                    return true;
                }
                List<ItemStack> kit = kits.get(kitname.toLowerCase());
                for (ItemStack item : kit)
                    player.getInventory().addItem(item);
                player.sendMessage(String.format("%sНабор %s%s%s выдан.", ChatColor.YELLOW, ChatColor.WHITE, kitname, ChatColor.YELLOW));
                enteredCode(player.getName(), kitname);
                return true;
            } else {
                int hours = timePassedHours(player.getName(), kitname);
                player.sendMessage(String.format("%sВы сможете получить набор %s%s%s через %s%s часов.", ChatColor.RED, ChatColor.WHITE, kitname, ChatColor.RED, ChatColor.WHITE, hours));
                return true;
            }
        } catch (NullPointerException e){
            player.sendMessage(String.format("%sНабор %s%s%s не существует.", ChatColor.RED, ChatColor.WHITE, kitname, ChatColor.RED));
            e.printStackTrace();
            return true;
        }

    }

    private static void enteredCode (String name, String kitname){
        String time = SDF.format(Calendar.getInstance().getTime());
        lastTaken.put(name.toLowerCase() + "#" + kitname.toLowerCase(), time);
        File kitLogFile = new File("plugins/BurnageCommands", "kit.log");
        BufferedWriter buffW;
        try {
            buffW = new BufferedWriter(new FileWriter(kitLogFile));
            for (String namekit : lastTaken.keySet()){
                buffW.write(namekit.toLowerCase() + "#" + lastTaken.get(namekit) +"\n");
            }
            buffW.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }



    private static void loadKits () throws IOException {
        File directory = new File("plugins/BurnageCommands/kits/");
        if (!directory.exists()) {
            directory.mkdirs();
            Bukkit.getLogger().info("Directory 'kit/' created.");
            return;
        }
        File[] files = directory.listFiles();
        for (File f : files){
            BufferedReader bufferedReader = new BufferedReader(new FileReader(f));
            String line;
            List <ItemStack> kit = new ArrayList<>();
            while ((line = bufferedReader.readLine()) != null) {
                String [] u = line.split("#");
                try {
                    ItemStack item = new ItemStack(Integer.valueOf(u[0]), Integer.valueOf(u[1]));
                    kit.add(item);
                } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
                    try {
                        ItemStack item = new ItemStack(Integer.valueOf(u[0]));
                        kit.add(item);
                    } catch (NullPointerException ignore){

                    }
                }
            }
            kits.put(f.getName().toLowerCase(), kit);
            Bukkit.getLogger().info("[BurnCMD] Kit " + f.getName().toLowerCase() + " loaded.");
            bufferedReader.close();
        }

        File kitLogFile = new File("plugins/BurnageCommands", "kit.log");
        try {
            BufferedReader br = new BufferedReader(new FileReader(kitLogFile));
            String line;
            while ((line = br.readLine()) != null){
                try {
                    String[] u = line.split("#");
                    String namekit = u[0] + "#" + u[1];
                    lastTaken.put(namekit.toLowerCase(), u[2].toLowerCase());
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
            br.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    static {
        try {
            loadKits();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
