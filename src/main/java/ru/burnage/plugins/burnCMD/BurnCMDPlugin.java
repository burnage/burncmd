package ru.burnage.plugins.burnCMD;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import ru.burnage.plugins.ManagerPVP;
import ru.burnage.plugins.PlayerPVPEvent;
import ru.burnage.plugins.burnCMD.admin.VanishManager;
import ru.burnage.plugins.burnCMD.admin.commands.GameModeCMD;
import ru.burnage.plugins.burnCMD.admin.commands.OpenInvCMD;
import ru.burnage.plugins.burnCMD.admin.commands.VanishCMD;
import ru.burnage.plugins.burnCMD.other.FlyManager;
import ru.burnage.plugins.burnCMD.other.GodManager;
import ru.burnage.plugins.burnCMD.other.commands.FlyCMD;
import ru.burnage.plugins.burnCMD.other.commands.GodCMD;
import ru.burnage.plugins.burnCMD.other.commands.KitCMD;
import ru.burnage.plugins.burnCMD.teleports.HomeManager;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;
import ru.burnage.plugins.burnCMD.teleports.commands.*;
import ru.burnage.plugins.burnCMD.world.commands.TimeCMD;
import ru.burnage.plugins.burnCMD.world.commands.WeatherCMD;
import ru.burnage.plugins.chat_manager.PrivateMessageEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class BurnCMDPlugin extends JavaPlugin implements Listener {
    private HashMap<String, Drop> inv = new HashMap<>();

    private class Drop {
        private List<ItemStack> drops;

        public Drop (List<ItemStack>  drops){
            this.drops = drops;
        }

        public List<ItemStack>  getDrops (){
            return this.drops;
        }
    }

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getLogger().info("[BurnCMD]Plugin has started");

		this.getCommand("teleport").setExecutor(new TeleportCMD());
		this.getCommand("vanish").setExecutor(new VanishCMD());
		this.getCommand("gamemode").setExecutor(new GameModeCMD());
		this.getCommand("weather").setExecutor(new WeatherCMD());
		this.getCommand("bring").setExecutor(new BringCMD());
        this.getCommand("fly").setExecutor(new FlyCMD());
        this.getCommand("time").setExecutor(new TimeCMD());
        this.getCommand("home").setExecutor(new HomeCMD());
        this.getCommand("sethome").setExecutor(new SethomeCMD());
        this.getCommand("deletehome").setExecutor(new DeleteHomeCMD());
        this.getCommand("setspawn").setExecutor(new SetSpawnCMD());
        this.getCommand("spawn").setExecutor(new SpawnCMD());
        this.getCommand("warp").setExecutor(new WarpCMD());
        this.getCommand("createwarp").setExecutor(new WarpCreateCMD());
        this.getCommand("deletewarp").setExecutor(new DeleteWarpCMD());
        this.getCommand("god").setExecutor(new GodCMD());
        this.getCommand("warps").setExecutor(new WarpsCMD());
        this.getCommand("warpinfo").setExecutor(new WarpInfoCMD());
        this.getCommand("openinventory").setExecutor(new OpenInvCMD());
        this.getCommand("homes").setExecutor(new HomesCMD());
        this.getCommand("kit").setExecutor(new KitCMD());
        /**
         * this.getCommand("heal").setExecutor(new GodCMD());
         * this.getCommand("slap").setExecutor(new GodCMD());
         * this.getCommand("playertime").setExecutor(new GodCMD());
         * this.getCommand("warplist").setExecutor(new GodCMD());
         * this.getCommand("warpmessage").setExecutor(new GodCMD());
         */


        /**
         * TODO: warplist
         * TODO: warpmessage
         * TODO: warpinfo
         * TODO: heal
         * TODO: give
         * TODO: slap
         * TODO: kit
         * TODO: playertime
         */

        VanishManager.reset();
        FlyManager.clearSet();
        HomeManager.loadHomes();
        TeleportManager.loadWarp();

        try {
            TeleportManager.reloadSpawnLocation();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	@EventHandler(priority=EventPriority.HIGH)
	public void onJoin (PlayerJoinEvent e) throws IOException {
		VanishManager.playerJoinManager(e.getPlayer());
        if(!e.getPlayer().hasPlayedBefore()){
            TeleportManager.teleportToSpawn(e.getPlayer());
        }
        if(!e.getPlayer().hasPermission("burncmd.gamemode") && e.getPlayer().getGameMode().equals(GameMode.CREATIVE)){
            e.getPlayer().setGameMode(GameMode.SURVIVAL);
        }
        if(e.getPlayer().hasPermission("burncmd.god")){
            GodManager.setGod(e.getPlayer().getName());
        } else {
            GodManager.removeGod(e.getPlayer().getName());
        }
        if (FlyManager.isFly(e.getPlayer()))
            FlyManager.changeFly(e.getPlayer());
	}

    @EventHandler(priority=EventPriority.HIGH)
    public void onRespawn (PlayerRespawnEvent event) throws IOException {
        Collection<PotionEffect> ef = event.getPlayer().getActivePotionEffects();
        Player player = event.getPlayer();
        for (PotionEffect e : ef) {
            player.removePotionEffect(e.getType());
        }
        if(HomeManager.hasHome(player, "default")){
            player.teleport(HomeManager.getLoc(player, "default"));
        } else {
            TeleportManager.teleportToSpawn(player);
        }

        if (player.hasPermission("burncmd.saveinventory") && inv.containsKey(player.getName())){
            for (ItemStack item : inv.get(player.getName()).getDrops()) {
                if (item == null)
                    continue;
                player.getInventory().addItem(item);
            }
            player.sendMessage(ChatColor.AQUA + "Ваш инвентарь восстановлен.");
            inv.remove(player.getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDamage(EntityDamageEvent event){
        if(event.getEntity() instanceof Player){
            Player pl = ((Player) event.getEntity()).getPlayer();
            if(GodManager.isGod(pl.getName())){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPVP (PlayerPVPEvent event) {
        if (GodManager.isGod(event.getDamager().getName()) || GodManager.isGod(event.getDefender().getName())){
            event.setCancelled(true);
            event.getDamager().sendMessage(ChatColor.RED + "Один из двух игроков находится в режиме бессмертия.");
            return;
        }

        if (FlyManager.isFly(event.getDamager()) || FlyManager.isFly(event.getDefender())){
            event.setCancelled(true);
            event.getDamager().sendMessage(ChatColor.RED + "Один из двух игроков находится в режиме полета.");
        }

        if (VanishManager.isVanished(event.getDamager().getName())){
            event.setCancelled(true);
            event.getDamager().sendMessage(ChatColor.RED + "Вы находитесь в режиме невидимости.");
        }
    }

    @EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onChat (PrivateMessageEvent event){
        if (VanishManager.isVanished(event.getGettter()) && !Bukkit.getPlayer(event.getSender()).hasPermission("burncmd.vanish.see")){
            event.setCancelled(true, "Игрока нет в сети.");
        }
    }

    @EventHandler (priority = EventPriority.MONITOR)
    public void onPickup (PlayerPickupItemEvent event){
        if (VanishManager.isVanished(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onDeath (PlayerDeathEvent event){
        Player player = event.getEntity().getPlayer();
        if (player.hasPermission("burncmd.saveinventory")){
            if (player.getKiller() instanceof Player || ManagerPVP.isFight(player)){
                player.sendMessage(ChatColor.AQUA + "Ваш инвентарь не будет восстановлен, так как вы погибли сражаясь.");
                return;
            }

            PlayerInventory playerInventory = player.getInventory();
            List <ItemStack> itemStacks = new ArrayList<>();
            for (ItemStack i : playerInventory)
                itemStacks.add(i);
            inv.put(player.getName(),new Drop(itemStacks));
            event.getDrops().clear();
        }
    }
}
