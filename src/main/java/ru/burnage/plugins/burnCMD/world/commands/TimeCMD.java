package ru.burnage.plugins.burnCMD.world.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.CharConversionException;
import java.util.Collection;

public class TimeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
        World world = Bukkit.getPlayer(sender.getName()).getWorld();

        if(args.length == 0){
            sender.sendMessage(ChatColor.YELLOW + "Игровое время: " + ChatColor.WHITE + world.getTime());
            return true;
        }

        if(sender.hasPermission("burncmd.time.world")){
            switch (args[0]){
                case "set":
                    timeSet(world,args[1], sender);
                    break;
                default:
                    timeSet(world, args[0], sender);
            }
        } else {
            sender.sendMessage(ChatColor.RED + "У вас нет прав!");
        }
        return true;
    }

    private void timeSet(World world, String time, CommandSender sender){
        Collection<? extends Player> players = Bukkit.getOnlinePlayers();
        if (!checkString(time)){
            switch (time){
                case "day":
                    world.setTime(0);
                    for (Player player : players){
                        player.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " установил " + ChatColor.YELLOW + "день" + ChatColor.GREEN + " в мире.");
                    }
                    break;
                case "night":
                    world.setTime(20000);
                    for (Player player : players){
                        player.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " установил " + ChatColor.YELLOW + "ночь" + ChatColor.GREEN + " в мире.");
                    }
                    break;
                default:
                    sender.sendMessage(ChatColor.RED + "Неизвестный аргумент!");
            }
        } else {
            long t = Integer.valueOf(time);
            if(t > 0 && t < 40000){
                world.setTime(t);
                for (Player player : players){
                    player.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " установил время: " + ChatColor.YELLOW + t + ChatColor.GREEN + " в мире.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Неправильный формат времени!");
            }
        }
    }

    private boolean checkString(String string) {
        if (string == null) return false;
        return string.matches("^-?\\d+$");
    }
}
