package ru.burnage.plugins.burnCMD.world.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.goldshop.GShopAPI;
import ru.burnage.plugins.goldshop.TradeType;

import java.util.Collection;

public class WeatherCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
		if (arg3.length == 0){
			return false;
		}
		Collection<? extends Player> players;
		if (sender.hasPermission("burncmd.weather")){

			World world = Bukkit.getPlayer(sender.getName()).getWorld();
			switch (arg3[0].toLowerCase()){
			case "sunny":
				world.setStorm(false);
				players = Bukkit.getOnlinePlayers();
				for (Player player : players){
					player.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " остановил дождь!");
				}
				
				return true;
			case "stormy":
            case "rainy":
				world.setStorm(true);
				players = Bukkit.getOnlinePlayers();
				for (Player player : players){
					player.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " начал дождь!");
				}
				return true;
			default:
				sender.sendMessage(ChatColor.RED + "Ошибка в написании погоды!");
				return true;
			}
		} else {
			switch (arg3[0].toLowerCase()) {
				case "sunny":
					TradeType type = TradeType.CHANGE_WEATHER;
					String[] settings = new String[10];
					settings[0] = sender.getName();
					settings[1] = arg3[0];
					type.set(settings);
					GShopAPI.invoice("Вы покупаете смену погоды на солнечную", sender.getName(), type);
					break;
				case "stormy":
				case "rainy":
					type = TradeType.CHANGE_WEATHER;
					settings = new String[10];
					settings[0] = sender.getName();
					settings[1] = arg3[0];
					type.set(settings);
					GShopAPI.invoice("Вы покупаете смену погоды на дождливую", sender.getName(), type);
					break;
				default:
					sender.sendMessage(ChatColor.RED + "Ошибка в написании погоды!");
					return true;
			}
		}
		return true;
	}
}
