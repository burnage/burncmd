package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;

public class SetSpawnCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if(sender.hasPermission("burncmd.teleport.setspawn")){
            TeleportManager.setSpawn(Bukkit.getPlayer(sender.getName()).getLocation(), Bukkit.getPlayer(sender.getName()).getWorld());
            sender.sendMessage(ChatColor.GREEN + "Локация спавна успешно изменена!");
        } else {
            sender.sendMessage(ChatColor.RED + "У вас нет прав!");
        }

        return true;
    }
}
