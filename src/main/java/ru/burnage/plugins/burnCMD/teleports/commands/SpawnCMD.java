package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SpawnCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        try {
            TeleportManager.reloadSpawnLocation();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            TeleportManager.teleportToSpawn(Bukkit.getPlayer(sender.getName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sender.sendMessage(ChatColor.GREEN + "Телепортация на спавн выполнена успешно!");
        return true;
    }
}
