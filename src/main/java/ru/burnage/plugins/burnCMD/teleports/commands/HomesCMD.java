package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.teleports.Home;
import ru.burnage.plugins.burnCMD.teleports.HomeManager;

import java.util.Set;

public class HomesCMD implements CommandExecutor {
    CommandSender sender;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        this.sender = sender;
        if (!(sender instanceof Player))
            return error("Команда запрещена из консоли");
        if (args.length == 0){
            return showHomes(sender.getName());
        } else if (args.length == 1){
            if (args[0].equalsIgnoreCase(sender.getName()))
                return showHomes(args[0]);
            if (sender.hasPermission("burncmd.home.other"))
                return showHomes(args[0]);
            return error("У Вас нет прав просматривать дома других игроков.");
        } else {
            return false;
        }
    }

    private boolean error (String text){
        sender.sendMessage(ChatColor.RED + text);
        return true;
    }

    private boolean showHomes (String player){
        Set<Home> homes = HomeManager.getHomes(player);
        if (homes == null || homes.size() == 0)
            if (player.equalsIgnoreCase(sender.getName()))
                return error("У Вас нет домов");
            else
                return error(String.format("У игрока %s нет домов.", player));
        sender.sendMessage(ChatColor.DARK_BLUE + "Дома игрока " + ChatColor.YELLOW + player);
        for (Home home : homes)
            sender.sendMessage(ChatColor.YELLOW + "-" + home.getHomeName());
        return true;
    }
}
