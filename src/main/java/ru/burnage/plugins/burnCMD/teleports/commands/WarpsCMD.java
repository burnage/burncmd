package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;
import ru.burnage.plugins.burnCMD.teleports.Warp;

public class WarpsCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        int page = 1;
        if (args.length != 0){
            try {
                page = Integer.valueOf(args[0]);
            }catch (Exception e){
                return false;
            }
        }
        int t = 0;
        Warp [] list = new Warp[TeleportManager.getWarps().size()];
        for (Warp w : TeleportManager.getWarps()){
            list[t] = w;
            t ++;
        }
        if ((page <= 0) ){
            sender.sendMessage(ChatColor.RED + "Такой страницы не существует!");
            return true;
        }

        String [] result = new String[10];
        int temp = 0;
        for (t = 10*(page-1); t < 10*page; t ++){
            if (t >= TeleportManager.getWarps().size())
                break;
            result [temp] = ChatColor.YELLOW + list[t].warpName;
            temp++;
        }
        if (temp == 0)
            sender.sendMessage(ChatColor.RED + "Такой страницы не существует!");
        else {
            sender.sendMessage(ChatColor.YELLOW + "Список варпов. Страница " + page);
            for (String w : result) {
                if (w == null)
                    continue;
                sender.sendMessage(w);
            }
        }
        return true;
    }
}
