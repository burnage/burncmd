package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.teleports.HomeManager;

public class DeleteHomeCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
        if (args.length == 0){
            HomeManager.removeHome(sender.getName(), "default");
            sender.sendMessage(ChatColor.GREEN + "Стандартный дом удален!");
        } else {
            HomeManager.removeHome(sender.getName(), args[0]);
            sender.sendMessage(ChatColor.GREEN + "Дом " + ChatColor.YELLOW + args[0] + ChatColor.GREEN + " удален!");
        }
        return true;
    }
}
