package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;
import ru.burnage.plugins.burnCMD.teleports.Warp;

public class WarpInfoCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length == 0)
            return false;
        Warp warp = TeleportManager.getWarp(args[0]);
        if (warp != null){
            sender.sendMessage(ChatColor.YELLOW + "Информация о варпе " + warp.warpName);
            sender.sendMessage(ChatColor.YELLOW + "Координаты: " + String.format("{%s, %s, %s}", warp.location.getBlockX(), warp.location.getBlockY(), warp.location.getBlockZ()));
            sender.sendMessage(ChatColor.YELLOW + "Создатель: " + warp.creator);
        }
        return true;
    }
}
