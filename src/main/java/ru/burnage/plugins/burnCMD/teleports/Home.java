package ru.burnage.plugins.burnCMD.teleports;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;



public class Home {
    private String ownerName;
    private String homename;
    public float x;
    public float y;
    public float z;
    public float pitch;
    public float yaw;
    public World world;
    public Location location;

    public Home (String homeowner, String homename, float x, float y, float z, float pitch, float yaw, World world){
        this.homename = homename;
        this.ownerName = homeowner;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
        this.world = world;
        location = new Location(world, x, y, z);
        location.setPitch(pitch);
        location.setYaw(yaw);
    }

    public Home (String homeowner, String homename, Location location){
        this.homename = homename;
        this.ownerName = homeowner;
        this.x = (float) location.getX();
        this.y = (float) location.getY();
        this.z = (float) location.getZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
        this.world = location.getWorld();
        this.location = location;
    }

    public void teleportHome (Player player){
        TeleportManager.teleport(player, location);
    }

    public String getHomeName (){
        return homename;
    }

    public String getOwnerName (){
        return ownerName;
    }
}
