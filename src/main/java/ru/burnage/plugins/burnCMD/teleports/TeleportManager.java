package ru.burnage.plugins.burnCMD.teleports;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.io.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TeleportManager {
    private static Location spawn;
    private static Set<Warp> warps = new HashSet<Warp>();

    public static Set<Warp> getWarps (){
        return warps;
    }

    public static void reloadSpawnLocation () throws IOException {
        File file = new File("plugins/BurnageCommands","spawn.txt");
        BufferedReader buffR;

        buffR = new BufferedReader(new FileReader(file));
        String line = buffR.readLine();
        if(line.length() <1)
            return;
        String [] lol = line.split("_");//x, y, z, pitch, yaw, worldtype
        World w = Bukkit.getWorld(lol[5]); //.substring(16, lol[5].length() - 1)); //Пляски с бубном, но работает! хз почему, но если использовать тот же алгоритм с варпами, получается херня.
        spawn = new Location(w, Float.valueOf(lol[0]), Float.valueOf(lol[1]), Float.valueOf(lol[2]));
        spawn.setPitch(Float.valueOf(lol[3]));
        spawn.setYaw(Float.valueOf(lol[4]));
        buffR.close();

    }

    public static void setSpawn (Location playerLocation, World w){
        spawn = playerLocation;

        File file = new File("plugins/BurnageCommands","spawn.txt");
        BufferedWriter buffW;
        try{
            buffW = new BufferedWriter(new FileWriter(file));
            buffW.write(spawn.getX() + "_" + spawn.getY() + "_" + spawn.getZ() + "_" + spawn.getPitch() + "_" + spawn.getYaw() + "_" + spawn.getWorld().getName() +"\n");
            buffW.close();
            w.setSpawnLocation((int) spawn.getX(), (int) spawn.getY(), (int) spawn.getZ());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void teleportToSpawn (Player player) throws IOException {
        reloadSpawnLocation();
        teleport(player, spawn);
    }
	
	public static void teleport (Player player, int x, int y, int z){
		player.teleport(new Location(player.getWorld(), x,y,z));
	}
	
	public static void teleport (Player player, Player loc){
		player.teleport(loc);
	}

    public static void teleport (Player player, Location loc){
        player.teleport(loc);
    }

    public static void teleport (Player player, int x, int y, int z, float pitch, float yaw){
        Location loc = new Location(player.getWorld(), x,y,z);
        loc.setPitch(pitch);
        loc.setYaw(yaw);
        player.teleport(loc);
    }

    public static void teleport (Player player, float x, float y, float z, float pitch, float yaw){
        Location loc = new Location(player.getWorld(), x,y,z);
        loc.setPitch(pitch);
        loc.setYaw(yaw);
        player.teleport(loc);
    }

    public static void loadWarp ()  {
        warps.clear();
        File file = new File("plugins/BurnageCommands","warps.txt");
        String line;
        String[] wtf;
        BufferedReader buffR;
        try {
            buffR = new BufferedReader(new FileReader(file));
            while ((line = buffR.readLine()) != null) {
                wtf = line.split("#");
                Warp warp = new Warp(wtf[0].toLowerCase(), wtf[1].toLowerCase(), Float.valueOf(wtf[2]), Float.valueOf(wtf[3]), Float.valueOf(wtf[4]), Float.valueOf(wtf[5]), Float.valueOf(wtf[6]), Bukkit.getWorld(wtf[7]));
                warps.add(warp);
            }
            buffR.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static boolean teleportToWarp (Player player, String warpName){
        for (Warp fuck : warps) {
            if (fuck.warpName.toLowerCase().equals(warpName.toLowerCase())) {
                player.teleport(fuck.location);
                return true;
            }
        }
        return false;
    }

    public static Warp getWarp (String warpName){
        for (Warp fuck : warps) {
            if (fuck.warpName.toLowerCase().equals(warpName.toLowerCase())) {
                return fuck;
            }
        }
        return null;
    }

    public static void createWarp (Player player, String warpName){
        Warp newWarp = new Warp(player.getName(), warpName, player.getLocation());
        warps.add(newWarp);

        File file = new File("plugins/BurnageCommands","warps.txt");
        BufferedWriter buffW;
        Iterator<Warp> itr;
        try{
            buffW = new BufferedWriter(new FileWriter(file));
            itr = warps.iterator();
            while (itr.hasNext()){
                newWarp = itr.next();
                buffW.write(newWarp.creator + "#" + newWarp.warpName + "#" + newWarp.location.getX() + "#" + newWarp.location.getY() + "#" + newWarp.location.getZ() + "#" + newWarp.location.getPitch() + "#" + newWarp.location.getYaw() + "#" + newWarp.location.getWorld().getName() +"\n");
            }
            buffW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createWarp (Warp newWarp){
        warps.add(newWarp);

        File file = new File("plugins/BurnageCommands","warps.txt");
        BufferedWriter buffW;
        Iterator<Warp> itr;
        try{
            buffW = new BufferedWriter(new FileWriter(file));
            itr = warps.iterator();
            while (itr.hasNext()){
                newWarp = itr.next();
                buffW.write(newWarp.creator + "#" + newWarp.warpName + "#" + newWarp.location.getX() + "#" + newWarp.location.getY() + "#" + newWarp.location.getZ() + "#" + newWarp.location.getPitch() + "#" + newWarp.location.getYaw() + "#" + newWarp.location.getWorld().getName() +"\n");
            }
            buffW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isWarpExist (String warpname){
        for (Warp warp : warps)
            if (warp.warpName.equalsIgnoreCase(warpname))
                return true;
        return false;
    }

    public static void deleteWarp(Warp warp) {
        warps.remove(warp);
        File file = new File("plugins/BurnageCommands","warps.txt");
        BufferedWriter buffW;
        Iterator<Warp> itr;
        try{
            buffW = new BufferedWriter(new FileWriter(file));
            itr = warps.iterator();
            while (itr.hasNext()){
                Warp warpItr = itr.next();
                buffW.write(warpItr.creator + "#" + warpItr.warpName + "#" + warpItr.location.getX() + "#" + warpItr.location.getY() + "#" + warpItr.location.getZ() + "#" + warpItr.location.getPitch() + "#" + warpItr.location.getYaw() + "#" + warpItr.location.getWorld().getName() +"\n");
            }
            buffW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
