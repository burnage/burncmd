package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.admin.VanishManager;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;

public class BringCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
		if (args.length == 0){
			return false;
			
		}
		if (sender.hasPermission("burncmd.teleport.other")){
            if (Bukkit.getPlayer(args[0]) == null){
                sender.sendMessage(ChatColor.RED + "Данного игрока нет онлайн!");
                return true;
            }
            if(VanishManager.isVanished(args[0]) && !sender.hasPermission("burncmd.vanish.see")){
                sender.sendMessage(ChatColor.RED + "Данного игрока нет онлайн!");
                return true;
            }
			TeleportManager.teleport(Bukkit.getPlayer(args[0]), Bukkit.getPlayer(sender.getName()));
            sender.sendMessage(ChatColor.GREEN + "К вам телепортирован "+ ChatColor.YELLOW + Bukkit.getPlayer(args[0]).getName());
            Bukkit.getPlayer(args[0]).sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " телепортировал вас к себе.");
			
		} else {
			sender.sendMessage(ChatColor.RED + "У вас нет прав!");
		}
		return true;
	}
}
