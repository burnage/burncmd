package ru.burnage.plugins.burnCMD.teleports;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class HomeManager {
    private static Set<Home> homes = new HashSet<>();

    public static void loadHomes (){
        File file = new File("plugins/BurnageCommands","homes.txt");
        BufferedReader buffR;
        String line;
        String[] wtf;
        homes.clear();

        try {
            buffR = new BufferedReader(new FileReader(file));
            while ((line = buffR.readLine()) != null) {
                wtf = line.split("#");
                homes.add(new Home(wtf[0].toLowerCase(), wtf[1].toLowerCase(), Float.valueOf(wtf[2]), Float.valueOf(wtf[3]), Float.valueOf(wtf[4]), Float.valueOf(wtf[5]), Float.valueOf(wtf[6]), Bukkit.getWorld(wtf[7])));
            }
            buffR.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void teleportHome (String homeName, Player player){
        for (Home h : homes) {
            if (h.getHomeName().toLowerCase().equals(homeName.toLowerCase()) && h.getOwnerName().toLowerCase().equals(player.getName().toLowerCase())) {
                h.teleportHome(player);
                if (homeName.toLowerCase().equals("default")) {
                    player.sendMessage(ChatColor.GREEN + "Вы телепортировались в свой стандартный дом");
                } else {
                    player.sendMessage(ChatColor.GREEN + "Вы телепортировались в свой дом " + ChatColor.YELLOW + homeName + ChatColor.GREEN + ".");
                }
                return;
            }
        }
        player.sendMessage(ChatColor.RED + "Дом не найден!");
    }

    public static void teleportHome (String playerName, String homeName, Player player){
        for (Home h : homes) {
            if (h.getHomeName().toLowerCase().equals(homeName.toLowerCase()) && h.getOwnerName().toLowerCase().equals(playerName.toLowerCase())) {
                h.teleportHome(player);
                if (homeName.toLowerCase().equals("default")) {
                    player.sendMessage(ChatColor.GREEN + "Вы телепортировались в стадартный дом игрока " + ChatColor.YELLOW + playerName + ChatColor.GREEN + ".");
                } else {
                    player.sendMessage(ChatColor.GREEN + "Вы телепортировались в дом " + ChatColor.YELLOW + homeName + ChatColor.GREEN + " игрока " + ChatColor.YELLOW + playerName + ChatColor.GREEN + ".");
                }
                return;
            }
        }
        player.sendMessage(ChatColor.RED + "Дом не найден!");
    }

    public static void createHome (Player player, String homeName){
        float x, y, z;
        x = (float) player.getLocation().getX();
        y = (float) player.getLocation().getY();
        z = (float) player.getLocation().getZ();
        Home setHome = new  Home (player.getName(), homeName, x, y, z, player.getLocation().getPitch(), player.getLocation().getYaw(), player.getWorld());
        homes.add(setHome);

        File file = new File("plugins/BurnageCommands","homes.txt");
        BufferedWriter buffW;
        Iterator<Home> itr;
        try{
            buffW = new BufferedWriter(new FileWriter(file));
            itr = homes.iterator();
            while (itr.hasNext()){
                setHome = itr.next();
                buffW.write(setHome.getOwnerName() + "#" + setHome.getHomeName() + "#" + setHome.x + "#" + setHome.y + "#" + setHome.z + "#" + setHome.pitch + "#" + setHome.yaw + "#" + setHome.world.getName() +"\n");
            }
            buffW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int countHomes (String playername){//выводит доп. кол-во домов
        int num = 0;

        for (Home h : homes) {
            if (h.getOwnerName().equalsIgnoreCase(playername) && !(h.getHomeName().equalsIgnoreCase("default"))) {
                num++;
            }
        }

        return num;
    }

    public static Set<Home> getHomes (String playername){
        Set <Home> playerHomes = new HashSet<>();
        for (Home home : homes)
            if (home.getOwnerName().equalsIgnoreCase(playername))
                playerHomes.add(home);
        return playerHomes;
    }

    public static void createHome (Player player){
        createHome(player, "default");
    }

    public static void createHome (Home home){
        homes.add(home);

        File file = new File("plugins/BurnageCommands","homes.txt");
        BufferedWriter buffW;
        Iterator<Home> itr;
        try{
            buffW = new BufferedWriter(new FileWriter(file));
            itr = homes.iterator();
            while (itr.hasNext()){
                home = itr.next();
                buffW.write(home.getOwnerName() + "#" + home.getHomeName() + "#" + home.x + "#" + home.y + "#" + home.z + "#" + home.pitch + "#" + home.yaw + "#" + home.world.getName() +"\n");
            }
            buffW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean removeHome (String playerName, String homeName){
        Home lol = findHome(playerName, homeName);
        if(lol == null){
            return false;
        }
        homes.remove(lol);

        File file = new File("plugins/BurnageCommands","homes.txt");
        BufferedWriter buffW;
        Iterator<Home> itr;
        Home setHome;
        try{
            buffW = new BufferedWriter(new FileWriter(file));
            itr = homes.iterator();
            while (itr.hasNext()){
                setHome = itr.next();
                buffW.write(setHome.getOwnerName() + "#" + setHome.getHomeName() + "#" + setHome.x + "#" + setHome.y + "#" + setHome.z +  "#" + setHome.pitch + "#" + setHome.yaw + "#" +  setHome.world.getName() +"\n");
            }
            buffW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static Home findHome(String ownerName, String homeName){
        Iterator<Home> itr;
        itr = homes.iterator();
        while (itr.hasNext()){
            Home lol = itr.next();
            if(lol.getHomeName().toLowerCase().equals(homeName.toLowerCase()) && lol.getOwnerName().toLowerCase().equals(ownerName.toLowerCase())){
                return lol;
            }
        }
        return null;
    }

    public static boolean hasHome(Player player, String homename){
        for (Home home : homes){
            String owner, name;
            owner = home.getOwnerName();
            name = home.getHomeName();
            if(name.toLowerCase().equals(homename.toLowerCase()) && owner.toLowerCase().equals(player.getName().toLowerCase())){
                return true;
            }
        }

        return false;
    }

    public static Location getLoc(Player player, String homename){
        Home home;
        Location loc;
        Iterator<Home> itr = homes.iterator();
        String owner, name;
        while (itr.hasNext()){
            home = itr.next();
            if(home.getHomeName().toLowerCase().equals(homename.toLowerCase()) && home.getOwnerName().toLowerCase().equals(player.getName().toLowerCase())){
                loc = new Location(Bukkit.getWorld("world"), home.x, home.y, home.z);
                loc.setPitch(home.pitch);
                loc.setYaw(home.yaw);
                return loc;
            }
        }
        return null;
    }
}
