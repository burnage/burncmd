package ru.burnage.plugins.burnCMD.teleports;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class Warp {
    public String creator;
    public String warpName;
    public Location location;

    public Warp (String creator, String name, float x, float y, float z, float pitch, float yaw, World world){
        this.creator = creator;
        warpName = name;
        location = new Location(world, x, y, z);
        location.setPitch(pitch);
        location.setYaw(yaw);
    }

    public Warp (String creator, String name, Location loc){
        this.creator = creator;
        warpName = name;
        location = loc;
    }
}
