package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;
import ru.burnage.plugins.burnCMD.teleports.Warp;

public class DeleteWarpCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
        if(args.length == 0){
            sender.sendMessage(ChatColor.RED + "Не указано указано название варпа!");
            return true;
        }
        Warp warp = TeleportManager.getWarp(args[0]);
        if (warp == null){
            sender.sendMessage(ChatColor.RED + "Варп не найден!");
            return true;
        }
        if (sender.hasPermission("burncmd.warp.delete") || warp.creator.equalsIgnoreCase(sender.getName())){
            TeleportManager.deleteWarp(warp);
            sender.sendMessage(ChatColor.YELLOW + "Варп " + warp.warpName + " успешно удален.");
        } else {
            sender.sendMessage(ChatColor.RED + "Вы не можете удалить не свой варп.");
        }
        return true;
    }
}
