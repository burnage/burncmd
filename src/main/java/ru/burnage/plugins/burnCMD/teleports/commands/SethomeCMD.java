package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.teleports.Home;
import ru.burnage.plugins.burnCMD.teleports.HomeCreationEvent;
import ru.burnage.plugins.burnCMD.teleports.HomeManager;
import ru.burnage.plugins.goldshop.GShopAPI;
import ru.burnage.plugins.goldshop.Trade;
import ru.burnage.plugins.goldshop.TradeType;
import ru.med.premcontroller.main.PermControllerAPI;

public class SethomeCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
        if (args.length == 0){
            HomeCreationEvent event = new HomeCreationEvent(new Home(sender.getName(), "default", ((Player) sender).getLocation()));
            Bukkit.getServer().getPluginManager().callEvent(event);
            if (event.isCancelled()){
                sender.sendMessage(ChatColor.RED + event.getError_message());
                return true;
            }
            HomeManager.removeHome(sender.getName(), "default");
            HomeManager.createHome(Bukkit.getPlayer(sender.getName()));
            sender.sendMessage(ChatColor.GREEN + "Стандартный дом установлен!");
        } else {
            String plGroup = PermControllerAPI.getGroup(sender.getName().toLowerCase());
            int count;
            switch (plGroup){
                case "MODA":
                case "MODB":
                case "MODC":
                    count = 5;
                    break;
                case "ADMA":
                case "ADMB":
                    count = 10;
                    break;
                default:
                    count = 0;
            }

            if (PermControllerAPI.isGold(sender.getName().toLowerCase()))
                count += 5;
            if ((PermControllerAPI.isPremium(sender.getName().toLowerCase())))
                count += 3;

            if(HomeManager.countHomes(sender.getName()) >= count){
                if(!sender.hasPermission("burncmd.home.infinity")){
                    Bukkit.getLogger().info(String.valueOf(HomeManager.countHomes(sender.getName())));
                    Bukkit.getLogger().info(String.valueOf(count));
                    TradeType tradeType = TradeType.HOME;
                    String[] settings = new String[10];
                    settings[0] = sender.getName();
                    settings[1] = args [0];
                    settings[2] = ((Player) sender).getWorld().getName();
                    settings[3] = String.valueOf(((Player) sender).getLocation().getX());
                    settings[4] = String.valueOf(((Player) sender).getLocation().getY());
                    settings[5] = String.valueOf(((Player) sender).getLocation().getZ());
                    settings[6] = String.valueOf(((Player) sender).getLocation().getPitch());
                    settings[7] = String.valueOf(((Player) sender).getLocation().getYaw());
                    tradeType.set(settings);
                    GShopAPI.invoice("Вы покупаете установку дома  " + args[0], sender.getName(), tradeType);
                    return true;
                }
            }
            HomeCreationEvent event = new HomeCreationEvent(new Home(sender.getName(), args[0], ((Player) sender).getLocation()));
            Bukkit.getServer().getPluginManager().callEvent(event);
            if (event.isCancelled()){
                sender.sendMessage(ChatColor.RED + event.getError_message());
                return true;
            }

            HomeManager.removeHome(sender.getName(), args[0]);
            HomeManager.createHome(Bukkit.getPlayer(sender.getName()), args[0]);
            sender.sendMessage(ChatColor.GREEN + "Дом " + ChatColor.YELLOW + args[0] + ChatColor.GREEN + " установлен!");
        }
        return true;
    }
}
