package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.teleports.HomeManager;

public class HomeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
            if(args.length == 0){
                HomeManager.teleportHome("default", Bukkit.getPlayer(sender.getName()));
            } else {
                String[] wtf = args[0].split(":");
                if (wtf.length == 1){
                    HomeManager.teleportHome(wtf[0], Bukkit.getPlayer(sender.getName()));
                } else {
                    if(sender.hasPermission("burncmd.home.other")){
                        HomeManager.teleportHome(wtf[0], wtf[1], Bukkit.getPlayer(sender.getName()));
                    } else {
                        sender.sendMessage(ChatColor.RED + "У вас нет прав!");
                    }
                }
            }
        return true;
    }
}
