package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;
import ru.burnage.plugins.goldshop.GShopAPI;
import ru.burnage.plugins.goldshop.TradeType;

public class WarpCreateCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }

        if (args.length == 0){
            sender.sendMessage(ChatColor.RED + "Не указано название варпа!");
            return true;
        }

        if (TeleportManager.isWarpExist(args[0])){
            sender.sendMessage(ChatColor.RED + "Такой варп уже существует.");
            return true;
        }

        if(!sender.hasPermission("burncmd.warp.create")){
            TradeType type = TradeType.WARP;
            String[] settings = new String[10];
            settings[0] = sender.getName();
            settings[1] = args [0];
            settings[2] = ((Player) sender).getWorld().getName();
            settings[3] = String.valueOf(((Player) sender).getLocation().getX());
            settings[4] = String.valueOf(((Player) sender).getLocation().getY());
            settings[5] = String.valueOf(((Player) sender).getLocation().getZ());
            settings[6] = String.valueOf(((Player) sender).getLocation().getPitch());
            settings[7] = String.valueOf(((Player) sender).getLocation().getYaw());
            type.set(settings);
            GShopAPI.invoice("Вы покупаете установку варпа " + args[0], sender.getName(), type);
            return true;
        }

        TeleportManager.createWarp(Bukkit.getPlayer(sender.getName()), args[0]);
        sender.sendMessage(ChatColor.GREEN + "Варп " + ChatColor.YELLOW + args[0] + ChatColor.GREEN + " успешно создан!");

        return true;
    }
}
