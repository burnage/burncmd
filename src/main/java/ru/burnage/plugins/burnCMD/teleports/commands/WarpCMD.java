package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.burnage.plugins.burnCMD.teleports.TeleportManager;

public class WarpCMD implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)){
            sender.sendMessage("Команда недоступна из консоли");
            return true;
        }
        if(args.length == 0){
            sender.sendMessage(ChatColor.RED + "Не указано указано название варпа!");
            return true;
        }
        boolean b = TeleportManager.teleportToWarp(Bukkit.getPlayer(sender.getName()), args[0]);
        TeleportManager.teleportToWarp(Bukkit.getPlayer(sender.getName()), args[0]);
        if (b)
            sender.sendMessage(ChatColor.GREEN + "Вы успешно телепортировались на варп " + ChatColor.YELLOW + args[0]);
        else
            sender.sendMessage(ChatColor.RED + "Варп не найден!");
        return true;
    }
}
