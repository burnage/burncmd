package ru.burnage.plugins.burnCMD.teleports.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.burnage.plugins.burnCMD.teleports.TeleportManager;
import ru.burnage.plugins.burnCMD.admin.VanishManager;

public class TeleportCMD implements CommandExecutor {
	private Player player;

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

        if(args.length == 1){
            if (!(sender instanceof Player)){
                sender.sendMessage("Команда недоступна из консоли");
                return true;
            }
            if (sender.hasPermission("burncmd.teleport")){
                Player pl = Bukkit.getPlayer(args[0].toLowerCase());
                if (pl == null){
                    sender.sendMessage(ChatColor.RED + "Данного игрока нет онлайн!");
                    return true;
                }
                if(!VanishManager.isVanished(pl.getName()) || sender.hasPermission("burncmd.vanish.see")){
                    TeleportManager.teleport(Bukkit.getPlayer(sender.getName()), pl);
                    sender.sendMessage(ChatColor.GREEN+"Вы телепортировались к "+ChatColor.YELLOW + pl.getName());
                } else {
                    sender.sendMessage(ChatColor.RED + "Данного игрока нет онлайн!");
                    return true;
                }
                if(!VanishManager.isVanished(sender.getName()))
                    pl.sendMessage(ChatColor.YELLOW+"К вам телепортировался "+ChatColor.GREEN + sender.getName());
            } else {
                sender.sendMessage(ChatColor.RED + "У Вас нет прав!");
            }
            return true;
        } else if (args.length == 2){
            if (!sender.hasPermission("burncmd.teleport.other")){
                sender.sendMessage(ChatColor.RED + "У вас нет прав!");
                return true;
            }
            Player pl1 = Bukkit.getPlayer(args[0]);
            Player pl2 = Bukkit.getPlayer(args[1]);
            if ((pl1 == null) || (pl2 == null)){
                sender.sendMessage(ChatColor.RED + "Одного из двух игроков нет онлайн!");
                return true;
            }
            if(VanishManager.isVanished(pl1.getName()) || VanishManager.isVanished(pl2.getName())){
                sender.sendMessage(ChatColor.RED + "Одного из двух игроков нет онлайн!");
                return true;
            }
            TeleportManager.teleport(pl1, pl2);
            pl1.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " телепортировал вас к " + ChatColor.YELLOW + pl2.getName());
            pl2.sendMessage(ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + " телепортировал к вам " + ChatColor.YELLOW + pl1.getName());
            sender.sendMessage(ChatColor.GREEN + "Телепортация игрока " + ChatColor.YELLOW + pl1.getName() +ChatColor.GREEN + " к игроку " + ChatColor.YELLOW + pl2.getName() + ChatColor.GREEN + " выполнена успешно!");
            return true;
        } else if (args.length == 3) {
            if (!(sender instanceof Player)){
                sender.sendMessage("Команда недоступна из консоли");
                return true;
            }
            if (!sender.hasPermission("burncmd.teleport")){
                sender.sendMessage(ChatColor.RED + "У Вас нет прав!");
            }
            if(checkString(args[0])&&checkString(args[1])&&checkString(args[2])){
                int x = Integer.valueOf(args[0]);
                int y = Integer.valueOf(args[1]);
                int z = Integer.valueOf(args[2]);
                TeleportManager.teleport(Bukkit.getPlayer(sender.getName()), x, y, z);
                sender.sendMessage(ChatColor.GREEN + "Произведена телепортация по координатам: " + ChatColor.YELLOW + x + ChatColor.GREEN + ", " + ChatColor.YELLOW + y + ChatColor.GREEN + ", "+ ChatColor.YELLOW + z + ChatColor.YELLOW);
            } else {
                sender.sendMessage(ChatColor.RED + "Некорректные координаты!");
            }
            return true;
        } else {
                sender.sendMessage(ChatColor.RED + "Неверное количество аргументов!");
            return true;
        }
	}

    private boolean checkString(String string) {
        if (string == null) return false;
        return string.matches("^-?\\d+$");
    }
}
